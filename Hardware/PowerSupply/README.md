# PowerSupply for RedPitaya Console

The PowerSupply is made in two stages 
1. Mains to 24 VDC. The 24 Volts are accessible via the banana connectors on front and back panel. <br>![alt text](MainsConverter.png "24 V AC/DC Converter")

2. A custom PCB is designed to deliver 5 V for the RedPitaya and 3.3 V, -15 V and +15 V for the OCRA1. It is powered by the 24 VDC mains converter (stage 1.) <br>![alt text](PowerSupply_PCB.png "PCB PowerSuply")

## Schematic of PCB

![alt text](PowerSupply_PCB_schematic.png "Schematic of custom power supply PCB")

## BOM

| **ld** 	| **Designator** 	| **Description**           	| **Manufacturer PartNo** 	| **Quantity** 	| **Link**                                                                                        	|
|--------	|----------------	|---------------------------	|-------------------------	|--------------	|-------------------------------------------------------------------------------------------------	|
| 1      	| C1, C2, C3     	| Cap 10 uF 50V 1206        	| GRT31CR61H106KE01L      	| 3            	| https://de.farnell.com/en-DE/murata/grt31cr61h106ke01l/cap-10-f-50v-10-x5r-1206/dp/2672214      	|
| 2      	| C4, C5         	| Cap 470 pF 2 kV 1206      	|  C1206X471KGRACTU       	| 2            	| https://de.farnell.com/en-DE/kemet/c1206x471kgractu/cap-470pf-2-kv-10-x7r-1206/dp/2676515       	|
| 3      	| L1, L2         	| Inductor 15 uH            	| LPS4018-153MRB          	| 2            	| https://de.farnell.com/en-DE/coilcraft/lps4018-153mrb/inductor-15uh-20-0-8a-shld-smd/dp/2408106 	|
| 4      	| J0             	| Screw Terminal 3 Contacts 	| CTB0700/3               	| 1            	| https://de.farnell.com/en-DE/camdenboss/ctb0700-3/terminal-block-wire-to-brd-3pos/dp/1717085    	|
| 5      	| J1             	| Screw Terminal 4 Contacts 	| CTB0100/4               	| 1            	| https://de.farnell.com/en-DE/camdenboss/ctb0100-4/pcb-terminal-5mm-4p/dp/3378874                	|
| 6      	| J2             	| USB A                     	| 67643-0910              	| 1            	| https://de.farnell.com/en-DE/molex/67643-0910/usb-conn-2-0-usb-type-a-rcpt-tht/dp/1450206       	|
| 7      	| PS1            	| 5V 10W                    	| THD 10-2411N            	| 1            	| https://de.farnell.com/en-DE/tracopower/thd-10-2411n/dc-dc-converter-5-1v-2a/dp/2813156         	|
| 8      	| PS2            	| 3V3                       	| TRN 1-2410              	| 1            	| https://de.farnell.com/en-DE/tracopower/trn-1-2410/dc-dc-converter-3-3v-0-3a/dp/2829840         	|
| 9      	| PS3            	| +- 15 V                   	| ITQ2415S-H              	| 1            	| https://de.farnell.com/en-DE/xp-power/itq2415s-h/dc-dc-converter-2-o-p-6w-sip/dp/2484153        	|
| 10     	| -              	| 24 V AC/DC Converter      	| IRM-60-24ST             	| 1            	| https://de.farnell.com/en-DE/mean-well/irm-60-24st/power-supply-ac-dc-24v-2-5a/dp/3253389       	|


