# OSI² low-field MRI - Console Hardware

This repository describes one way of building an open-source console for magnetic resonance imaging in a standard 19" 1 HU rack.

It is based on
- RedPitaya https://redpitaya.com/sdrlab-122-16/
- OCRA1 https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/


![alt text](front.jpeg "Red-Pitaya based MRI Console")

## Bill of Materials

| **ld** 	| **Description**                      	| **Manufacturer PartNo** 	| **needed Quantity** 	| **Qty per Package** 	| **Order Qty** 	| **Link**                                                                                                                       	|
|--------	|--------------------------------------	|-------------------------	|---------------------	|---------------------	|---------------	|--------------------------------------------------------------------------------------------------------------------------------	|
| 1      	| Schroff Case 1HU                     	| 20860-601               	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/schroff/20860-601/case-19-rack-1u-220mm-aluminium/dp/1816025                                      	|
| 2      	| Schroff handle                       	| JR-101-1FS              	| 2                   	| 2                   	| 1             	| https://de.farnell.com/en-DE/nvent-schroff/20860-256/handles-1u-pk2/dp/1370447                                                 	|
| 3      	| Mains plug                           	| JR-101-1FS              	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/multicomp/jr-101-1fs/inlet-iec-fused-snap-fit/dp/9521623                                          	|
| 4      	| Fuse                                 	| 0239002.MXP             	| 2                   	| 10                  	| 1             	| https://de.farnell.com/en-DE/littelfuse/0239002-mxp/cartridge-fuse-slow-blow-2a-250v/dp/1596646                                	|
| 5      	| Power Switch                         	| C1353AT0/1GRN           	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/arcolectric/c1353at0-1grn/switch-dpst-green-i-o-16a-250v/dp/521942                                	|
| 6      	| 6.3mm plug                           	| 2-520184-4              	|           9         	| 10                  	| 1             	| https://de.farnell.com/en-DE/amp-te-connectivity/2-520184-4/terminal-female-disconnect-22/dp/2293338                           	|
| 7      	| Banana Connector Black               	| 972354100               	| 2                   	| 1                   	| 2             	| https://de.farnell.com/en-DE/hirschmann-testmeasurement/972354100/banana-socket-32a-4mm-panel-black/dp/1011401                 	|
| 8      	| Banana Connector Red                 	| 972354101               	| 2                   	| 1                   	| 2             	| https://de.farnell.com/en-DE/hirschmann-testmeasurement/972354101/banana-socket-32a-4mm-panel-red/dp/1011402                   	|
| 9      	| Ethernet Panel Mount                 	| 9454521560              	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/harting/09454521560/adaptor-rj45-cat6-panel/dp/1972965                                            	|
| 10     	| Ethernet Patch Cable                 	| 2965-0.3BK              	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/videk/2965-0-3bk/patch-lead-cat5e-black-0-3m/dp/1526202                                           	|
| 11     	| SMB Jacks                            	| R114553000              	| 8                   	| 1                   	| 8             	| https://de.farnell.com/en-DE/radiall/r114553000/rf-coaxial-smb-straight-jack-50ohm/dp/4194524                                  	|
| 12     	| GPIO cable set                       	| MP006290                	| 8                   	| 20                  	| 1             	| https://de.farnell.com/en-DE/multicomp-pro/mp006290/jumper-wire-kit-female-to-female/dp/3617779                                	|
| 13     	| Fuse holder                          	| 3101.002                	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/schurter/3101-0020/fuse-holder-5x20mm-slot-knob-panel/dp/1134125                                  	|
| 14     	| 8.5 mm Standoff                      	| TRFAHSSFFM2.5-8.5-5     	|      8              	| 50                  	| 1             	| https://de.farnell.com/en-DE/tr-fastenings/trfahssffm2-5-8-5-5/standoff-hex-female-steel-8-5mm/dp/2505986                      	|
| 15     	| 6 mm Standoff 	| 	R25-1000602      	|         4            	| 1                  	| 4             	| https://de.farnell.com/en-DE/harwin/r25-1000602/standoff-hex-female-brass-m2-5/dp/3754475                        	|
| 16     	| Machine Screw M2.5, Countersunk      	| MP006516                	|              12       	| 100                 	| 1             	| https://de.farnell.com/en-DE/multicomp-pro/mp006516/screw-flat-csk-head-pozidriv-m2/dp/3666708                                 	|
| 17     	| Machine Screw M2.5                   	| MP006578                	|       12              	| 100                 	| 1             	| https://de.farnell.com/en-DE/multicomp-pro/mp006578/screw-pan-head-pozidriv-m2-5-6mm/dp/3666777                                	|
| 18     	| Screw Countersunk M3                 	| M312 KRA2MCS100-        	|     4               	| 100                 	| 1             	| https://de.farnell.com/en-DE/tr-fastenings/m312-kra2mcs100/screw-pozi-csk-s-s-a2-m3x12-pk100/dp/1420423                        	|
| 19     	| Nut M3                               	|                         	| 4                   	|                     	|               	|                                                                                                                                	|
| 20     	| Washer M3                            	|                         	| 4                   	|                     	|               	|                                                                                                                                	|
| 21     	| Washer M2.5                          	| MP008054                	|                     	| 200                 	| 1             	| https://de.farnell.com/en-DE/multicomp-pro/mp008054/washer-flat-nylon-m2-5-od-5mm/dp/3804765                                   	|
| 22     	| Nut for BNC plugs                    	| 1-329631-2              	| 6                   	| 10                  	| 1             	| https://de.farnell.com/en-DE/amp-te-connectivity/1-329631-2/nut-bnc-connector-lead-jam-nut/dp/1651010                          	|
| 23     	| BNC Washer                           	| 		1-329632-2              	| 6                   	| 10                  	| 1             	| https://de.farnell.com/en-DE/amp-te-connectivity/1-329632-2/lockwasher/dp/1651011                                              	|
| 24     	| USB Cable                            	| 11.02.8310              	| 1                   	| 1                   	| 1             	| https://de.farnell.com/en-DE/roline/11-02-8310/computer-cable-usb2-0-150mm-black/dp/2444222                                    	|
| 25     	| Cable Tie Mount                      	| 151-28701               	|                     	| 50                  	| 1             	| https://de.farnell.com/en-DE/hellermanntyton/mb3a-blk-50-pack/cable-tie-mount-4-1mm-pa66-pk50/dp/1169045                       	|
| 26     	| RedPitaya                            	| SDRlab 122-16           	| 1                   	| 1                   	| 1             	| https://redpitaya.com/sdrlab-122-16/                                                                                           	|
| 27     	| Gradient DAC                         	| OCRA1                   	| 1                   	| 1                   	| 1             	| https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/                       	|
| 29     	| PowerSupply                          	|                         	| 1                   	|                     	|               	| see PowerSupply/README.md                                                                                                  	|
| 30     	| SD-Card Extension Cable              	| 4251266700630           	| 1                   	| 1                   	| 1             	| https://www.reichelt.de/sg/de/raspberry-pi-microsd-verlaengerung-flexibel-15cm-rpi-msd-fl15-p223611.html?&trstct=vrt_pdn&nbc=1 	|
| 31     	| microSD Card Panelmount              	| please print it yourself       	|      1           	|           1         	|     1         	| https://www.printables.com/model/250146-microsd-extender-panel-mount                                                           	|
| 32     	| 26 Pin IDC Connector       	| D89126-0131HK         |  2                 	|       1              	| 2      	    | https://de.farnell.com/en-DE/3m/d89126-0131hk/connector-rcpt-26pos-2row-2-54mm/dp/2672511                           |
| 33     	| 26 Pin Cable       	| 63912615521CAB        |  1                 	|       1              	|        1       	| https://de.farnell.com/en-DE/wurth-elektronik/63912615521cab/unshld-ribbon-cable-26cond-28awg/dp/3583713                        |





## Building Instructions

1. Machine changes to the Schroff case. Milling is needed on front and back panel, for details refer to Casing/FreeCAD/*. There you can find FreeCAD Projects, step-files etc.

2. Drill the holes for the screws which will hold all the parts in place. For a drawing see (ToDo!)

3. Solder PowerSupply PCB, see PowerSupply/README.md

4. Assemble the Schroff case, except top.

5. Mount everything as in the picture. ToDo: Explosion Drawing <br> ![alt text](topview.jpeg "Top-view of Red-Pitaya based MRI Console")

6. Do the wiring according to the schematic: <br>![alt text](schematic.png "Schematic")

7. Commissioning: Before switching on and installing the software it is wisely to measure the voltage of each power input of each PCB before actually setting them under voltage. 

8. Mount the top plate and then continue the commissioning and installation as described in Software/MaRCoS/MaRCoS_QuickStartGuide.md.

9. Enjoy the build and your research!

Feel free to ask for help if anything is unclear. In case you have improvements, additional documents, drawings etc. please share them here or send us an email. <br>
Thanks a lot for your help to make this console and documentation better!


## Further Reading

Information about related projects and alternative hard- and software can be found here:
- https://doi.org/10.48550/arXiv.2208.01616
- https://github.com/vnegnev/marcos_extras/wiki
- https://zeugmatographix.org/ocra/
- https://www.opensourceimaging.org/



