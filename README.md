# OSI² low-field MRI - Console Red Pitaya based

## Description

Low-field MRI console based on the Red Pitaya


![alt text](Hardware/front.jpeg "Red-Pitaya based MRI Console")


## Specifications 

- Red-Pitaya SDRlab 122-16
- OCRA1 https://zeugmatographix.org/ocra/
- MaRCoS https://doi.org/10.48550/arXiv.2208.01616
- Passive TR-Switch (XXX Link)
- Preamps
- Casing + power supply

## Contacts

| Name          | Email                   | Institution                                                     | 
| ------------  | -----                   | -----------                                                     | 
| Jan Frintz  | jan.frintz@ptb.de  | Physikalisch-Technische Bundesanstalt (PTB), Berlin, Germany    |                               
| Lukas Winter  | lukas.winter@ptb.de     | Physikalisch-Technische Bundesanstalt (PTB), Berlin, Germany    |                

## Contributors (alphabetical order)

Jan Frintz, Tobias Mohr, Reiner Montag, David Schote, Frank Seifert, Berk Silemek, Lukas Winter

## License and Liability

Please check the [DISCLAIMER](./DISCLAIMER.pdf)

Following parts or subprojects are not licensed under CERN Open Hardware Licence Version 2 - Weakly Reciprocal and might be in conflict with it:

- OCRA1 by Marcus Prier, Forschungscampus STIMULATE, Otto-von-Guericke University, Magdeburg. Please refer to the following link for license details https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/
- The microSD extender panel mount https://www.printables.com/model/250146-microsd-extender-panel-mount was published by @MichaelOlson under CC BY 4.0 https://creativecommons.org/licenses/by/4.0/

- The RedPitaya SDRlab is proprietary hardware (only parts of the schematic are available).

